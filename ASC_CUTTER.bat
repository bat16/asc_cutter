@ECHO OFF
setlocal enabledelayedexpansion
ECHO CUT ASC TO BUFFER 
ECHO input ASC and shapefile buffer
ECHO Author: Szczepkowski Marek
ECHO Date: 07-04-2020
ECHO Version: 1.0
ECHO QGIS 2.14 - 3.8

SET QGIS_ROOT=C:\Program Files\QGIS 3.8
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET COORDINATE=2180


REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_CUTTED

REM Path to working dir
SET WORK=%cd%
if not exist "%WORK%\_CUTTED" mkdir "%WORK%\_CUTTED"

REM COUNTER FILES
dir /b *.asc 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

FOR /F %%i IN ('dir /b "%WORK%\*.asc"') DO (
    ECHO.
	ECHO. Processing  %%i    !Counter! / %count% FILES
	
	REM create index of tiled tif
	ECHO   CREATING TILE
	gdaltindex -tileindex location -f "ESRI Shapefile" "%WORK%\_CUTTED\tile.shp" "%WORK%\%%i"
	REM intersection tiled with buffer
	ogr2ogr -clipsrc "%WORK%\_CUTTED\tile.shp" "%WORK%\_CUTTED\cutter.shp" *.shp
	ECHO   CUTTING
	gdalwarp -co "TFW=YES" -of GTiff -s_srs EPSG:%COORDINATE% -t_srs EPSG:%COORDINATE% -dstnodata "0 0 0 " -cutline "%WORK%\_CUTTED\cutter.shp" "%WORK%\%%i" "%WORK%\_CUTTED\%%~ni%OUT_SUFFIX%.tif"
	gdal_translate -of AAIGrid "%WORK%\_CUTTED\%%~ni%OUT_SUFFIX%.tif" "%WORK%\_CUTTED\%%~ni%OUT_SUFFIX%.asc"
	
	
	REM delete files
	DEL /Q "%WORK%\_CUTTED\tile.*"
	DEL /Q "%WORK%\_CUTTED\cutter.*"
	DEL /Q "%WORK%\_CUTTED\%%~ni%OUT_SUFFIX%.tif"
	DEL /Q "%WORK%\_CUTTED\%%~ni%OUT_SUFFIX%.tfw"
	DEL /Q "%WORK%\_CUTTED\%%~ni%OUT_SUFFIX%.prj"
	DEL /Q "%WORK%\_CUTTED\%%~ni%OUT_SUFFIX%*.xml"
	set /A Counter+=1

)
ECHO.
ECHO Done
PAUSE